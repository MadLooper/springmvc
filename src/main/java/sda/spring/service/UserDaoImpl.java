package sda.spring.service;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import sda.spring.model.Login;
import sda.spring.model.User;

import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class UserDaoImpl implements UserDao {

    private ConcurrentHashMap<String, User> data = new ConcurrentHashMap<>();

    public Optional<String> register(User user) {
        StringBuilder errors = new StringBuilder();


        if (StringUtils.isEmpty(user.getUsername())) {
            errors.append("Username's missing,");
        }
        if (StringUtils.isEmpty(user.getFirstname())) {
            errors.append("Firstname's missing");
        }

        if (StringUtils.isEmpty(user.getPassword())) {
            errors.append("Password's missing");
        }

        if (StringUtils.isEmpty(user.getAddress())) {
            errors.append("Address's missing");
        }
        if (StringUtils.isEmpty(user.getEmail())) {
            errors.append("email's empty");
        }
        if (StringUtils.isEmpty(user.getLastname())) {
            errors.append("lastname's empty");
        }
        if (StringUtils.isEmpty(user.getPhone())) {
            errors.append("phone's empty");
        }
        String errorMsg = errors.toString();
        if (StringUtils.isEmpty(errorMsg) && data.get(user.getUsername()) == null){
            data.put(user.getUsername(), user);
            return Optional.empty();
        }
        return Optional.of(errorMsg);
    }

    public boolean validateUser(Login login) {
        User user = data.get(login.getUsername());
        if (user == null) {
            return false;
        }
        if (user.getPassword().equals(login.getPassword())) {
            return true;
        }
        return false;
    }

    @Override
    public User getUser(String login) {
        return data.get(login);
    }
}
