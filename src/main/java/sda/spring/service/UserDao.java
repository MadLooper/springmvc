package sda.spring.service;

import sda.spring.model.Login;
import sda.spring.model.User;

import java.util.Optional;
//DAO - Data Access Object is basically an object or an interface that provides access to an underlying database or any other persistence storage.
public interface UserDao {
	
	Optional<String> register(User user);

	boolean validateUser(Login login);

	User getUser(String login);
}