package sda.spring.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import sda.spring.model.Login;
import sda.spring.model.User;
import sda.spring.service.UserDao;

import static javax.swing.text.StyleConstants.ModelAttribute;
import static javax.swing.text.StyleConstants.ModelAttribute;
import static javax.swing.text.StyleConstants.ModelAttribute;
import static javax.swing.text.StyleConstants.ModelAttribute;
import static javax.swing.text.StyleConstants.ModelAttribute;

@Controller
public class LoginController {
	@Autowired
	UserDao userService;

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	//request mapping - identyfikuje metode obslugujaca zadanie oraz sygnalizuje, ze metoda powinna oblugiwac zadania ze sciezka /login
	public ModelAndView showLogin(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("login");
		//viewName - wstawiasz nazwe pliku jsp.
		mav.addObject("login", new Login());
		//nazwa pliku jsp
		return mav;
	}

	@RequestMapping(value = "/loginProcess", method = RequestMethod.POST)
	public ModelAndView loginProcess(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("login") Login login) {
		ModelAndView mav = null;
		boolean loginSucessful = userService.validateUser(login);
		if (loginSucessful) {
			mav = new ModelAndView("welcome");
			User user = userService.getUser(login.getUsername());
			mav.addObject("user", user);
		} else {
			mav = new ModelAndView("login");
			mav.addObject("message", "Username or Password is wrong!!");
			//attribute name - zeby bylo odwolanie w jsp.
		}
		return mav;
	}


}
