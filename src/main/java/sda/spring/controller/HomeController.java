package sda.spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import sda.spring.service.LoggedUsersService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class HomeController {
    @Autowired
    LoggedUsersService loggedUsersService;

    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public ModelAndView showLogin (HttpServletRequest request, HttpServletResponse response){
        ModelAndView mav = new ModelAndView("home");
        mav.addObject("users", loggedUsersService.getLoggedInUsers());
        return mav;
    }
}
